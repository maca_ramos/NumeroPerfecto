package cl.ubb.numeroperfecto;


import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;


import org.junit.Test;


public class numeroPerfectoTest {

	@Test
	public void testIngresaNumeroSeisRetornaVerdaderoNumeroPerfecto() {
		/*arrange*/
		numeroPerfecto num = new numeroPerfecto();
		boolean resultado;
		
		/*act*/
		resultado = num.perfecto(6);
		
		/*assert*/
		assertThat(resultado,is(true));
	}
	
	@Test
	public void testIngresaNumeroVeintiOchoRetornaVerdaderoNumeroPerfecto() {
		/*arrange*/
		numeroPerfecto num = new numeroPerfecto();
		boolean resultado;
		
		/*act*/
		resultado = num.perfecto(28);
		
		/*assert*/
		assertThat(resultado,is(true));
	}
	
	@Test
	public void testIngresaNumero496RetornaVerdaderoNumeroPerfecto() {
		/*arrange*/
		numeroPerfecto num = new numeroPerfecto();
		boolean resultado;
		
		/*act*/
		resultado = num.perfecto(496);
		
		/*assert*/
		assertThat(resultado,is(true));
	}

}
